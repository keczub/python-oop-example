from dataclasses import dataclass

from gasoline.gasoline import GasPortion
from gaspump.gaspump import GasPump


class UnwantedPedalPressure(ValueError):
    pass


@dataclass
class Pedal:
    connected_gas_pump: GasPump

    def press(
            self,
            how_hard: float,
            seconds: float,
    ) -> GasPortion:
        if seconds < 0:
            raise ValueError("No negative time period allowed")
        if how_hard < 0:
            raise UnwantedPedalPressure("No negative pressure allowed")
        how_hard = max(0, how_hard)
        how_hard = min(1, how_hard)
        return self.connected_gas_pump.apply(
            voltage=12*how_hard,
            seconds=seconds,
        )