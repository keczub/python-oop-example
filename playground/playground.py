from dataclasses import dataclass
from abc import ABC

# dataclasses
# https://docs.python.org/3/library/dataclasses.html


@dataclass
class InventoryItem:
    """Class for keeping track of an item in inventory."""
    name: str
    unit_price: float
    quantity_on_hand: int = 0

    def total_cost(self) -> float:
        return self.unit_price * self.quantity_on_hand


# valid object
item_1 = InventoryItem('Apple', 1.0, 2)
print(item_1)
print(item_1.total_cost())
# invalid object, incorrect datatypes
item_2 = InventoryItem(True, 100, 0.5)
print(item_2)
print(item_2.total_cost())


@dataclass
class Weapon:
    name: str
    damage: int


# valid object
item_3 = Weapon('blunt', 20)
print(item_3)


# abc — Abstract Base Classes
# https://docs.python.org/3/library/abc.html


class MyABC(ABC):
    pass


item_4 = MyABC()
print(item_4, type(item_4))


# typing is native, without typing library
# Pycharm informs in warning if datatype is incorrect

def foo(a: str, b: str) -> str:
    return a+b


print(foo("A", "B"))
print(foo(10, 20))


def foo2(number: int) -> str:
    if number == 0:
        return
    if number > 0:
        return 'OK'


print(foo2(0), type(foo2(0)))
print(foo2(2), type(foo2(2)))
